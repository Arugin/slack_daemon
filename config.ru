require './server'

class FiberSpawn
  def initialize(app)
    @app = app
  end

  def call(env)
    fiber = Fiber.new do
      res = @app.call(env)
      env['async.callback'].call(res)
    end
    EM.next_tick { fiber.resume }
    throw :async
  end
end

class ConnectionManagement
  def initialize(app)
    @app = app
  end

  def call(env)
    testing = env['rack.test']

    response = @app.call(env)
    response[2] = ::Rack::BodyProxy.new(response[2]) do
      ActiveRecord::Base.clear_active_connections! unless testing
    end

    response
  rescue Exception
    ActiveRecord::Base.clear_active_connections! unless testing
    raise
  end
end

use FiberSpawn
use ConnectionManagement
run SlackDaemon