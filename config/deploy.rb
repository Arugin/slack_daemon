# config valid only for current version of Capistrano
lock '3.14.1'

set :rbenv_ruby, '3.0.0'
set :rbenv_path, '/home/arugin/.rbenv/'
set :rbenv_map_bins, %w{rake gem bundle ruby rails whenever eye}

set :application, 'slack_daemon'
set :repo_url, 'git@bitbucket.org:Arugin/slack_daemon.git'

set :deploy_to, '/home/arugin/slack_daemon'
set :format, :pretty
set :default_shell, 'bash -l'

set :pty, true

set :rails_env, 'production'

set :linked_files, fetch(:linked_files, []).push('.env')
set :eye_application, fetch(:application)
set :eye_config, '/home/arugin/slack_daemon/current/slack_daemon.eye'
