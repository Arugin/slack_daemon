server 'unbegames.com', user: 'arugin', roles: %w{app web}

set :ssh_options, {
                    forward_agent: false,
                    auth_methods: ['publickey'],
                    keys: [ENV['AWS_SECRET_KEY_PATH']]
                }
