require 'bundler/setup'
require 'active_support/all'

environment = ENV['BOT_ENV'] || ENV['RAILS_ENV'] || 'development'

groups = { 'production' => [:default, :production], 'development' => [:default, :development],  'test' => [:default, :development, :test] }

Bundler.require *groups[environment]

relative_load_paths = %w[lib lib/models]
ActiveSupport::Dependencies.autoload_paths += relative_load_paths

dbconfig = YAML.load(ERB.new(File.read(File.expand_path('../../db/config.yml', __FILE__))).result)
ActiveRecord::Base.logger = Logger.new(STDERR) unless ENV['SERVER_ENV'] == 'test'
ActiveRecord::Base.establish_connection(dbconfig[environment])
