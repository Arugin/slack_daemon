class Users < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :telegram_id, limit: 8
      t.string :name
      t.string :username
      t.string :avatar
      t.string :access_token
    end
  end
end
