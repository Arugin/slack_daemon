class Insults < ActiveRecord::Migration[5.0]
  def change
    create_table :insults do |t|
      t.string :text
      t.string :category, default: 'general'
      t.string :locale, default: 'ru'
      t.boolean :approved, default: false
      t.references :user, index: true
    end
  end
end
