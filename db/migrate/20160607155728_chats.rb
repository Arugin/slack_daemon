class Chats < ActiveRecord::Migration[5.0]
  def change
    create_table :chats do |t|
      t.integer :chat_id, limit: 8
      t.string :title
      t.string :locale, default: 'ru'
      t.string :categories, array: true, default: []
      t.timestamps null: false
    end
  end
end
