class InsultStats < ActiveRecord::Migration[5.0]
  def change
    create_table :insult_stats, id: false do |t|
      t.integer :id, limit: 8, null: false
      t.integer :telegram_id, index: true
      t.integer :chat_id, index: true, limit: 8
      t.string :message

      t.references :insult, index: true
    end
  end
end
