class LayoutSwitcherOption < ActiveRecord::Migration[5.0]
  def change
    add_column :chats, :layout_switcher, :boolean, default: true
  end
end
