# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170210190247) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chats", id: :serial, force: :cascade do |t|
    t.bigint "chat_id"
    t.string "title"
    t.string "locale", default: "ru"
    t.string "categories", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "layout_switcher", default: true
  end

  create_table "insult_stats", id: false, force: :cascade do |t|
    t.bigint "id", null: false
    t.integer "telegram_id"
    t.bigint "chat_id"
    t.string "message"
    t.integer "insult_id"
    t.index ["chat_id"], name: "index_insult_stats_on_chat_id"
    t.index ["insult_id"], name: "index_insult_stats_on_insult_id"
    t.index ["telegram_id"], name: "index_insult_stats_on_telegram_id"
  end

  create_table "insults", id: :serial, force: :cascade do |t|
    t.string "text"
    t.string "category", default: "general"
    t.string "locale", default: "ru"
    t.boolean "approved", default: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_insults_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.bigint "telegram_id"
    t.string "name"
    t.string "username"
    t.string "avatar"
    t.string "access_token"
    t.boolean "admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
