Hi there!

I can help you to resolve the dispute in your Telegram chat. Just add it to the chat and type the `/dice` command with your opponent. And you will see, who was right.

I also listen all messages in the chat and automatically switches keyboard layout of message if it's wrong (currently eng->ru and ru->eng work).

Moreover I know all about the members of this chat. And sometimes (rarely) I will write all the truth about random selected member. Do you afraid that all dark corners of your friends's souls will open? If not — just add Slack Daemon to your chat!

You can also configure this chat by local and subscribe/unsubscribe it to different categories. See the commands bellow.

I have my own site. [I'll glad to see you on it](https://bot.unbe.ru)


*Supported commands*:
/help — Bot description and list of commands

/exrates — The dollar and the euro exchange rates
/dice — Random number from 1 to 20

/showconfig — show the config of the current chat
/subscribe <category name> — subscribe chat to the category with the given name
/unsubscribe <category name> — unsubscribe chat from the category with the given name
