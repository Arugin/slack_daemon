class BaseBot

  def initialize(token)
    @bot = AsyncTelegram::Bot.new(token)
    subscribe
  end

  def process(request)
    @bot.process(request)
  end

  def commands
    self.class::COMMANDS
  end

  def subscribe

  end

end
