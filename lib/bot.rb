class Bot < BaseBot

  COMMANDS = [Commands::Exchange, Commands::Help, Commands::Dice, Commands::DirectAnswer, Commands::RandomSmile]

  def initialize
    super ENV['TOKEN']
    @chats = {}
  end

  def fetch_chat(telegram_chat)
    return @chats[telegram_chat.id] if @chats[telegram_chat.id].present?
    chat = Chat.find_or_create_by(chat_id: telegram_chat.id)
    chat.title = telegram_chat.title || telegram_chat.username
    chat.categories = %w(General) if chat.categories.blank?
    chat.save
    @chats[telegram_chat.id] = chat
    chat
  end

  def subscribe
    commands.each do |command|
      command.subscribe(@bot)
    end

    @bot.on_text_message  do |message, api|
      chat = fetch_chat(message.chat)
      Commands::SwitchLayout.process(chat, message, api)
    end

    @bot.on_message do |message, api|
      chat = fetch_chat(message.chat)
      Commands::Insult.process(chat, message, api)
    end

    @bot.on_command_exec :showconfig do |message, api|
      chat = fetch_chat(message.chat)
      Commands::ShowConfig.process(chat, message, api)
    end

    @bot.on_command_exec :subscribe do |message, api|
      chat = fetch_chat(message.chat)
      Commands::Subscribe.process(chat, message, api)
    end

    @bot.on_command_exec :unsubscribe do |message, api|
      chat = fetch_chat(message.chat)
      Commands::Unsubscribe.process(chat, message, api)
    end

    @bot.on_command_exec :setlayoutswitcher do |message, api|
      chat = fetch_chat(message.chat)
      Commands::SetLayoutSwitcher.process(chat, message, api)
    end
  end
end
