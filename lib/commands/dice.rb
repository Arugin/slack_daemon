module Commands
  class Dice

    def self.subscribe(bot)
      bot.on_command_exec :dice do |message, api|
        api.send_message(chat_id: message.chat.id, text: Random.rand(20) + 1, reply_to_message_id: message.message_id)
      end
    end

  end
end
