module Commands
  class DirectAnswer

    MESSAGES = { '(ﾉಥ益ಥ）ﾉ﻿ ┻━┻' => '┬─┬ノ( º _ ºノ)',
                 '(╯°□°）╯︵ ┻━┻' => '┬─┬ノ( º _ ºノ)',
                 '┬─┬ノ( º _ ºノ)' => '¯\_(ツ)_/¯',
                 'иди нахуй' => 'сам иди',
                 'пошел нахуй' => 'сам туда пошел',
                 'бот иди нахуй' => 'хуй тебе' }

    def self.subscribe(bot)
      bot.on_text_message do |message, api|
        answer = MESSAGES[message.text]
        if answer.present?
          api.send_message(chat_id: message.chat.id, text: answer)
        end
      end
    end
  end
end
