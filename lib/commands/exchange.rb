module Commands
  class Exchange
    URL = 'https://www.cbr-xml-daily.ru/daily_json.js'
    VALUTES = %w(USD EUR GBP)

    def self.subscribe(bot)
      bot.on_command_exec :exrates do |message, api|
        http = EventMachine::HttpRequest.new(URL).get
        http.callback do
          results = String.new
          if http.response.blank?
            results = 'I\'m sorry API is unavailable'
          else
            Oj.load(http.response)['Valute'].select { |k,v| VALUTES.include?(k) }.each do |k,v|
              results << "1 #{k} = #{v['Value']} RUB \n"
            end
          end
          api.send_message(chat_id: message.chat.id, text: results)
        end
      end
    end

  end
end
