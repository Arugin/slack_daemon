module Commands
  class Help

    def self.help_text
      @help_text ||= File.read('doc/help.md')
    end

    def self.subscribe(bot)
      bot.on_command_exec :help do |message, api|
        api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: help_text, parse_mode: 'Markdown')
      end
    end
  end
end
