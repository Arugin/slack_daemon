module Commands
  class Insult

    def self.process(chat, message, api)
      if Random.rand(1000) < 5
        EM.defer do
          begin
            insult = ::Insult.where(category: chat.categories, locale: chat.locale).order('RANDOM()').limit(1).first
            if insult.present?
              ::InsultStat.create(telegram_id: message.from.id, chat_id: chat.chat_id, message: message.text, insult_id: insult.id)
              api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: insult.text, parse_mode: 'Markdown')
            end
          ensure
            ActiveRecord::Base.connection_pool.release_connection
          end
        end
      end
    end

  end
end
