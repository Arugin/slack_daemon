module Commands
  class Joke

    def self.request
      @http ||= EventMachine::HttpRequest.new('http://tambal.azurewebsites.net/joke/random')
    end

    def self.subscribe(bot)
      bot.on_text_message do |message, api|
        if Random.rand(1000) == 0
          send_joke(message, api)
        end
      end
    end

    private

    def self.send_joke(message, api)
      http = request.get
      http.callback do
        if http.response.blank?
          results = 'Я хотел чего-нибудь зашутить, но не получилось'
        else
          results = Oj.load(http.response)['joke']
        end
        api.send_message(chat_id: message.chat.id, text: results)
      end
    end

  end
end
