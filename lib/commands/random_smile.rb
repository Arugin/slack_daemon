module Commands
  class RandomSmile

    def self.smiles
      @smiles ||=  File.readlines('doc/smiles.txt')
    end

    def self.subscribe(bot)
      bot.on_message do |message, api|
        if Random.rand(10000) == 1
          api.send_message(chat_id: message.chat.id, text: smiles.sample)
        end
      end
    end
  end
end
