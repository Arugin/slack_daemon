module Commands
  class SetLayoutSwitcher

    def self.process(chat, message, api)
      option = message.text.sub('/setlayoutswitcher', '').strip == 'true'

      chat.update(layout_switcher: option)
    end
  end
end
