module Commands
  class ShowConfig
    def self.process(chat, message, api)
      api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: chat.to_md, parse_mode: 'Markdown')
    end
  end
end
