module Commands
  class Subscribe

    def self.process(chat, message, api)
      category = message.text.sub('/subscribe', '').strip.camelize

      if category.blank?
        api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Please, type the category name after `/subscribe` command', parse_mode: 'Markdown')
        return
      end

      if Chat::CATEGORIES.include?(category)
        if chat.categories.include?(category)
          api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "This chat is already subscribed to #{category} category")
        else
          chat.categories << category
          chat.save
          api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Done -y. Enjoy!')
        end
      else
        api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "I don't know this category: #{category}. Please select one of #{Chat::CATEGORIES.join(', ')}")
      end
    end

  end
end
