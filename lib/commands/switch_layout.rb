module Commands
  class SwitchLayout

    def self.corrector
      @corrector ||= KbdSwitcher::LayoutCorrector.new
    end

    def self.process(chat, message, api)
      return if message.text.size < 5
      return unless chat.layout_switcher
      
      text = corrector.correct(message.text)
      if text != message.text
        api.send_message(chat_id: message.chat.id, text: text, reply_to_message_id: message.message_id)
      end
    end
  end
end
