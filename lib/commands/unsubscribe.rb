module Commands
  class Unsubscribe

    def self.process(chat, message, api)
      category = message.text.sub('/unsubscribe', '').strip.camelize

      if category.blank?
        api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Please, type the category name after `/unsubscribe` command', parse_mode: 'Markdown')
        return
      end

      if chat.categories.include?(category)
        if chat.categories.count > 1
          chat.categories.delete(category)
          chat.save
          api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Done -y. =( ')
        else
          api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Really? You want to remove all categories? If you don\'t like my jokes, just kick me from this chat and don\'t fuck my mind.')
        end
      else
        api.send_message(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "This chat is not subscribed to #{category}. Please, select on of: #{chat.categories.join(', ')}")
      end

    end

  end
end
