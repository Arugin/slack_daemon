class Chat < ActiveRecord::Base
  CATEGORIES = %w(General Programming Hardcore Dota)

  validates_presence_of :chat_id

  def to_md
    <<-eos
Hi! This is chat settings:

*Chat title*: #{title}
*Categories*: #{categories.join(', ') }
*Locale*: #{locale}
*Layout Switcher*: #{layout_switcher}

You can add your own insult [here](https://bot.unbe.ru).

Type `/subscribe <category name>` to add new insult category to this chat.
Type `/unsubscribe <category name>` to revert this change.
Type `/setlayoutswitcher true|false` to enable or disable layout switcher.

Currently available categories: #{CATEGORIES.join(', ')}
    eos
  end
end
