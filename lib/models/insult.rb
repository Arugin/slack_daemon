class Insult < ActiveRecord::Base
  validates_presence_of :text, :category, :locale
end
