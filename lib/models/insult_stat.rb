class InsultStat < ActiveRecord::Base
  self.primary_key = 'id'
  auto_increment :id

  validates_presence_of :telegram_id, :chat_id, :insult_id
end