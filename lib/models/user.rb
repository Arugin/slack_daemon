class User < ActiveRecord::Base

  validates_presence_of :telegram_id, :access_token

  def full_name
    name || username || telegram_id
  end
end
