# frozen_string_literal: true

require 'dotenv'
require 'fiber'
Dotenv.load

require './config/environment'
require 'rack/csrf'
require 'openssl'

class SlackDaemon < Sinatra::Base
  register Sinatra::Partial

  configure :production, :development do
    set :bot, Bot.new
    enable :logging, :dump_errors, :raise_errors
    set :sprockets, Sprockets::Environment.new
    sprockets.append_path 'assets/stylesheets'
    sprockets.append_path 'assets/javascripts'
    sprockets.js_compressor  = :uglify
    sprockets.css_compressor = :scss
    set :partial_template_engine, :slim
    set :protection, except: :session_hijacking
    set :environment, (ENV['BOT_ENV'] || 'development').to_sym

    use Rack::Session::Cookie, key: 'rack.session',
        domain: ENV['BOT_ENV'] == 'production' ? 'bot.unbe.ru' : 'localhost',
        path: '/',
        expire_after: 2592000,
        secret: ENV['SESSION_SECRET']
  end

  helpers do
    def login_url
      "https://telegramlogin.com/token/#{ENV['LOGIN_CLIENT_ID']}?state=#{csrf_token}"
    end

    def csrf_token
      Rack::Csrf.csrf_token(env)
    end

    def csrf_tag
      Rack::Csrf.csrf_tag(env)
    end

    def current_user
      @user ||= User.find_by(access_token: env['rack.session']['access_token'])
    end

    def current_user=(user)
      env['rack.session']['access_token'] = user.access_token
    end

    def logged_in?
      current_user.present?
    end
  end

  post ENV['POST_PATH'] do
    settings.bot.process request
    'OK'
  end

  post ENV['POST_PICTURE_EATER'] do
    'OK'
  end

  get '/' do
    slim  :index
  end

  get '/login' do
    data = params.except(:hash).sort.to_h.map{|k,v| "#{k}=#{v}"}.join("\n")

    check = OpenSSL::HMAC.hexdigest('SHA256', Digest::SHA256.digest(ENV['TOKEN']), data)

    if check == params[:hash]
      user = User.find_or_create_by(telegram_id: params[:id])
      user.update_attributes(name: "#{params[:first_name]} #{params[:last_name]}", username: params[:username], avatar: params[:photo_url], access_token: params[:hash])
      self.current_user = user
      redirect '/suggestions'
    else
      p "Login failed: #{data} #{check} #{params[:hash]}"
      redirect '/'
    end
  end

  get '/suggest' do
    slim :suggest
  end

  get '/approve/:id' do
    fiber = Fiber.current
    if logged_in? && current_user.admin
      EM.defer(lambda {
        begin
          insult = Insult.find_by(id: params['id'])
          if insult.present?
            insult.update_attribute(:approved, true)
          end
        ensure
          ActiveRecord::Base.connection_pool.release_connection
        end
      }, lambda{ |result| fiber.resume(result) })
      Fiber.yield
      redirect 'admin'
    else
      redirect 'suggestions'
    end
  end

  get '/admin' do
    fiber = Fiber.current
    if logged_in? && current_user.admin
      insults = []
      EM.defer(lambda {
        begin
          insults = Insult.all.order(created_at: :desc)
        ensure
          ActiveRecord::Base.connection_pool.release_connection
        end
      }, lambda{ |result| fiber.resume(result) })
      Fiber.yield
      slim :suggestions, locals: { insults: insults }
    end
  end

  post '/suggest' do
    fiber = Fiber.current
    if logged_in? && params['_csrf'] == csrf_token
      insult = Insult.new(params['insult'])
      insult.user_id = current_user.id
      if current_user.admin
        insult.approved = true
      else
        insult.approved = false
      end
      EM.defer(lambda {
        begin
          insult.save
        ensure
          ActiveRecord::Base.connection_pool.release_connection
        end
      }, lambda{ |result| fiber.resume(result) })
      Fiber.yield
      redirect '/suggestions'
    end
  end

  get '/suggestions' do
    fiber = Fiber.current
    p "here"
    if logged_in?
      p "here 2"
      insults = []
      EM.defer(lambda {
        begin
          insults = Insult.where(user_id: current_user.id).order(created_at: :desc)
        ensure
          ActiveRecord::Base.connection_pool.release_connection
        end
      }, lambda{ |result| fiber.resume(result) })
      Fiber.yield
      slim :suggestions, locals: { insults: insults }
    end
  end

  get '/assets/*' do
    env['PATH_INFO'].sub!('/assets', '')
    settings.sprockets.call(env)
  end

end
