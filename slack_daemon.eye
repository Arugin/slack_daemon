Eye.application 'slack_daemon' do
  stdall 'log/trash.log'
  working_dir File.expand_path(File.dirname(__FILE__))
  env 'BOT_ENV' => 'production', 'RBENV_ROOT' => '~/.rbenv', 'RBENV_VERSION' => '3.0.0', 'PATH' => "~/.rbenv/shims:~/.rbenv/bin:#{ENV['PATH']}"
  trigger :flapping, times: 10, within: 1.minute, retry_in: 10.minutes
  check :cpu, every: 60.seconds, below: 100, times: 3
  stop_on_delete true

  process :thin do |p|
    daemonize true
    pid_file 'tmp/slack_daemon.pid'
    stdout 'log/server.log'
    start_command "bundle exec thin start -C config/thin.yml"
    stop_signals [:QUIT, 2.seconds, :TERM, 1.seconds, :KILL]
    check :socket, addr: 'unix:/home/arugin/tmp/slack_daemon.sock', every: 20.seconds, times: 2,
          timeout: 1.second
    check :memory, every: 30, below: 150.megabytes, times: 5
  end
end
